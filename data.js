const fs = require('fs');
const _ = require('lodash');

const storagePath = 'storage.json';

module.exports = class Data {
  static async getValueFromStorage(path) {
    const storageJSON = await fs.readFileSync(storagePath);
    let storage = {};

    try {
      storage = JSON.parse(storageJSON)
    } catch {
      storage = {};
    }

    if (path) {
      return _.get(storage, path)
    }

    return storage;
  }

  static async setValueToStorage(path, data) {
    const storage = await Data.getValueFromStorage();
    _.set(storage, path, data);

    await fs.writeFileSync(storagePath, JSON.stringify(storage))
  }
};
